CONTENTS OF THIS FILE
----------------------
 
  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers
 
 
INTRODUCTION
------------
 
Security Analysis is a module that audits a drupal project security parameters
to help development team but also leaders and managers in this scope.
It evaluates the security of headers, passwords and website fields
helping to prevent clickjacking, brute-force, XSS, code injection 
among others types of attacks based on in-house document
Security by Design in a clear, concise and user-friendly page.
 
 
REQUIREMENTS
------------
 
This module requires the module https://www.drupal.org/project/phpspreadsheet 
 
 
INSTALLATION
------------
 
  1. Copy security_analysis folder to modules directory
  2. At Administration >> Extend, enable the Security Analysis module
 
 
CONFIGURATION
-------------
 
There is no configuration page in this release
 
 
MAINTAINERS
-----------

Maintainer: 
icardoss(https://www.drupal.org/u/icardoss)
miguelpamferreira (https://www.drupal.org/u/miguelpamferreira)
bcruzcar(https://www.drupal.org/u/bcruzcar)
sara_asb(https://www.drupal.org/u/sara_asb)
nsalves(https://www.drupal.org/u/nsalves)
