<?php

namespace Drupal\security_analysis\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\security_review\Checklist;
use Drupal\security_review\SecurityReview;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Yaml\Yaml;

/**
 * Security controller.
 */
class SecurityController extends ControllerBase {

  /**
   * The security_review.checklist service.
   *
   * @var \Drupal\security_review\Checklist
   */
  protected $checklist;

  /**
   * The security_review service.
   *
   * @var \Drupal\security_review\SecurityReview
   */
  protected $securityReview;

  /**
   * The password_policy service.
   *
   * @var \Drupal\password_policy\PasswordInterface
   */
  protected $pass;

  /**
   * The current user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * The constructor.
   */
  public function __construct(SecurityReview $security_review = NULL, ?Checklist $checklist = NULL) {
    $this->securityReview = $security_review;
    $this->checklist = $checklist;
  }

  /**
   * Create service.
   */
  public static function create(ContainerInterface $container) {
    if ($container->has('security_review')) {
      return new static(
            $container->get('security_review'),
            $container->get('security_review.checklist'),
        );
    }
    if ($container->has('security_review') === FALSE) {
      return new static();
    }
  }

  /**
   * CHECK IF MODULE IS INSTALLED OR NOT.
   */
  public function checkModule($module) {
    $module_handler = \Drupal::moduleHandler();
    if ($module_handler->moduleExists($module)) {
      $module_status = 'INSTALLED';
    }
    else {
      $module_status = 'NOT INSTALLED';
    }
    return $module_status;
  }

  /**
   * GET RESULTS OBJECT SECURITY REVIEW MODULE AFTER RUN.
   */
  public function securityReviewResults() {
    $results = [];
    if ($this->checklist != NULL) {
      foreach ($this->checklist->getChecks() as $check) {
        $last_result = $check->lastResult();
        array_push($results, [$last_result->result(), $last_result->resultMessage()]);
      }
      return $results;
    }
  }

  /**
   * GET RESULTS OBJECT PASSWORD POLICY MODULE AFTER DEFINE POLICIES.
   */
  public function passwordPolicyResults() {
    $pass_results = \Drupal::service('entity_type.manager')->getStorage('password_policy')->loadMultiple();
    $passpolicy_results = [
      'password_reset' => $pass_reset,
      'password_length' => $pass_lenght,
      'password_policy_history_constraint' => $repeat_pass,
      'character_types' => $pass_character,
      'password_username' => $pass_username,
    ];
    $pass_names = ['password_reset',
      'password_length',
      'password_policy_history_constraint',
      'character_types',
      'password_username',
    ];
    if (count($pass_results) > 0) {
      for ($x = 0; $x < count($pass_results); $x++) {
        for ($y = 0; $y < count($pass_names); $y++) {
          if ((array_values($pass_results)[(int) $x]->get('policy_constraints')[0]['id']) === $pass_names[$y]) {
            $passpolicy_results[$pass_names[$y]] = array_values(array_values($pass_results)[(int) $x]->get('policy_constraints')[0])[1];
          }
        }
      }
      $passpolicy_results['password_reset'] = array_values($pass_results)[0]->get('password_reset');
    }
    $passpolicy_results = array_map(
          function ($value) {
              return ($value === NULL) ? 'not defined' : $value;
          }, $passpolicy_results
      );
    return $passpolicy_results;
  }

  /**
   * Download data to an excel file.
   */
  public function downloadFileExcel() {
    $rows = $this->buildata();
    $file_path = $rows['file_path'];

    $spreadsheet = new Spreadsheet();
    if (is_array($rows['rows_basic'])) {
      $this->tableInTab($spreadsheet,
            'INSTALLED MODULES',
            $rows['rows_basic'],
            [['Module', 'Status']]
          );
    }
    if (is_array($rows['rows_skit'])) {
      $this->tableInTab($spreadsheet,
            'SECURITY KIT',
            $rows['rows_skit'],
            [['Security Parameter', 'Actual Value', 'Advised Value', 'Ok/Not ok']]
          );
    }
    if (is_array($rows['rows_loginsec'])) {
      $this->tableInTab($spreadsheet,
            'LOGIN SECURITY',
            $rows['rows_loginsec'],
            [['Security Parameter', 'Actual Value', 'Advised Value', 'Ok/Not ok']]
          );
    }
    if (is_array($rows['rows_passpolicy'])) {
      $this->tableInTab($spreadsheet,
            'PASSWORD POLICY',
            $rows['rows_passpolicy'],
            [['Security Parameter', 'Actual Value', 'Advised Value', 'Ok/Not ok']]
          );
    }
    if (is_array($rows['rows_captcha'])) {
      $this->tableInTab($spreadsheet,
            'CAPTCHA',
            $rows['rows_captcha'],
            [['Security Parameter', 'Actual Value', 'Advised Value', 'Ok/Not ok']]
          );
    }
    if (is_array($rows['rows_sreview'])) {
      $this->tableInTab($spreadsheet,
            'SECURITY REVIEW',
            $rows['rows_sreview'],
            [['Security Parameter', 'Ok/Not ok']]
          );
    }

    // Create Excel file in the temporary path.
    $writer = new Xlsx($spreadsheet);
    $writer->save($file_path);

    // Return the Excel file as a response.
    $response = new BinaryFileResponse($file_path);
    $response->setContentDisposition(
          ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      );

    return $response;
  }

  /**
   * Build Table.
   */
  public function tableInTab($spreadsheet, $title, $data, $header) {
    $titleStyle = [
      'font' => [
        'bold' => TRUE,
      ],
      'fill' => [
        'fillType' => Fill::FILL_SOLID ,
        'startColor' => ['rgb' => 'C5D9F1'],
      ],
    ];
    if ($title == 'INSTALLED MODULES') {
      $sheet = $spreadsheet->getActiveSheet();
      $sheet->getStyle('A1:B1')->applyFromArray($titleStyle);
    }
    elseif ($title == 'SECURITY REVIEW') {
      $sheet = $spreadsheet->createSheet();
      $sheet->getStyle('A1:B1')->applyFromArray($titleStyle);
    }
    else {
      $sheet = $spreadsheet->createSheet();
      $sheet->getStyle('A1:D1')->applyFromArray($titleStyle);
    }
    $sheet->setTitle($title);

    $dataF = array_merge($header, $data);
    foreach ($dataF as $key => $row) {
      foreach ($row as $col => $value) {
        $sheet->setCellValueByColumnAndRow($col + 1, $key + 1, $value);
        $cell = $sheet->getCellByColumnAndRow($col + 1, $key + 1);
        $cell->getStyle()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $cell->getStyle()->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getColumnDimensionByColumn($col + 1)->setAutoSize(TRUE);
      }
    }
  }

  /**
   * Build data method.
   */
  public function buildata() {

    $status_sec_review = $this->checkModule('security_review');
    $module_cookies_policy = $this->checkModule('eu_cookies_policy');
    $module_pass_policy = $this->checkModule('password_policy');
    $module_seckit = $this->checkModule('seckit');
    $module_captcha = $this->checkModule('captcha');
    $module_recaptcha = $this->checkModule('recaptcha');
    $module_log_sec = $this->checkModule('login_security');

    // Security kit Ideal USING .YML file SAVED IN SPECIFIC FOLDER -extra.
    $module_path = \Drupal::moduleHandler()->getModule('security_analysis')->getPath();
    $file_path = $module_path . '/configs/seckit_ideal.settings.yml';
    $file_path = getcwd() . '/modules/custom/security_analysis/config/schema/seckit_ideal.settings.yml';
    $yaml_contents = file_get_contents($file_path);
    $yaml_data_seckit = Yaml::parse($yaml_contents);

    $configFactory = \Drupal::configFactory();

    // SECURITY KIT Ideal USING .YML FILES SAVED IN INSTALL DIRECTORY.
    // 1 - SAMEORIGIN.
    $config_xframe_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_clickjacking')["x_frame"];
    if ($config_xframe_ideal == 0) {
      $config_xframe_ideal = 'Disabled';
    };
    if ($config_xframe_ideal == 1) {
      $config_xframe_ideal = 'SameOrigin';
    };
    if ($config_xframe_ideal == 2) {
      $config_xframe_ideal = 'Deny';
    };
    if ($config_xframe_ideal == 3) {
      $config_xframe_ideal = 'Allow-FROM';
    };
    // 2 - 1; mode=block XSS filter
    $config_xss_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_xss')["x_xss"]["select"];
    if ($config_xss_ideal == 0) {
      $config_xss_ideal = 'Disabled';
    };
    if ($config_xss_ideal == 1) {
      $config_xss_ideal = '0';
    };
    if ($config_xss_ideal == 2) {
      $config_xss_ideal = '1; mode=block XSS filter';
    };
    if ($config_xss_ideal == 3) {
      $config_xss_ideal = '1';
    };
    // TRUE.
    $config_csp_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_xss')["csp"]["checkbox"];
    // TRUE.
    $config_ssl_transport_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_ssl')["hsts"];
    // 31536000
    $config_ssl_maxage_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_ssl')["hsts_max_age"];
    // TRUE.
    $config_ssl_subdomains_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_ssl')["hsts_subdomains"];
    // TRUE.
    $config_miscellaneous_referrer_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_various')["referrer_policy"];
    // Same origin.
    $config_miscellaneous_referrer_value_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_various')["referrer_policy_policy"];
    // TRUE.
    $config_miscellaneous_autocomplete_ideal = $configFactory->get('seckit_ideal.settings')->get('seckit_various')["disable_autocomplete"];

    // SECURITY KIT - Coonfigs applied on .yml.
    // 1 - SAMEORIGIN.
    $config_xframe = $configFactory->get('seckit.settings')->get('seckit_clickjacking')["x_frame"];
    if ($config_xframe == 0) {
      $config_xframe = 'Disabled';
    };
    if ($config_xframe == 1) {
      $config_xframe = 'SameOrigin';
    };
    if ($config_xframe == 2) {
      $config_xframe = 'Deny';
    };
    if ($config_xframe == 3) {
      $config_xframe = 'Allow-FROM';
    };
    // 2 - 1; mode=block XSS filter
    $config_xss = $configFactory->get('seckit.settings')->get('seckit_xss')["x_xss"]["select"];
    if ($config_xss == 0) {
      $config_xss = 'Disabled';
    };
    if ($config_xss == 1) {
      $config_xss = '0';
    };
    if ($config_xss == 2) {
      $config_xss = '1; mode=block XSS filter';
    };
    if ($config_xss == 3) {
      $config_xss = '1';
    };
    // TRUE.
    $config_csp = $configFactory->get('seckit.settings')->get('seckit_xss')["csp"]["checkbox"];
    // TRUE.
    $config_ssl_transport = $configFactory->get('seckit.settings')->get('seckit_ssl')["hsts"];
    // 31536000
    $config_ssl_maxage = $configFactory->get('seckit.settings')->get('seckit_ssl')["hsts_max_age"];
    // TRUE.
    $config_ssl_subdomains = $configFactory->get('seckit.settings')->get('seckit_ssl')["hsts_subdomains"];
    // TRUE.
    $config_miscellaneous_referrer = $configFactory->get('seckit.settings')->get('seckit_various')["referrer_policy"];
    // Same origin.
    $config_miscellaneous_referrer_value = $configFactory->get('seckit.settings')->get('seckit_various')["referrer_policy_policy"];
    // TRUE.
    $config_miscellaneous_autocomplete = $configFactory->get('seckit.settings')->get('seckit_various')["disable_autocomplete"];

    // LOGIN SECURITY IDEAL.
    // 4440.
    $config_tracktime_ideal = $configFactory->get('login_security_ideal.settings')->get('track_time');
    // 5
    $config_softhost_ideal = $configFactory->get('login_security_ideal.settings')->get('host_wrong_count');
    // 10
    $config_activity_ideal = $configFactory->get('login_security_ideal.settings')->get('activity_threshold');
    // Login Security based on .yml file
    // 4440.
    $config_tracktime = $configFactory->get('login_security.settings')->get('track_time');
    // 5
    $config_softhost = $configFactory->get('login_security.settings')->get('host_wrong_count');
    // 10
    $config_activity = $configFactory->get('login_security.settings')->get('activity_threshold');

    // Captcha based on .yml file.
    // 1.
    $config_enableform_ideal = $configFactory->get('captcha_ideal.settings')->get('enable_globally');
    if ($config_enableform_ideal == 0) {
      $config_enableform_ideal = 'false';
    };
    if ($config_enableform_ideal == 1) {
      $config_enableform_ideal = 'true';
    };
    // 0
    $config_casesensitive_ideal = $configFactory->get('captcha_ideal.settings')->get('default_validation');
    if ($config_casesensitive_ideal == 0) {
      $config_casesensitive_ideal = 'true';
    };
    if ($config_casesensitive_ideal == 1) {
      $config_casesensitive_ideal = 'false';
    };
    // 2
    $config_persistance_ideal = $configFactory->get('captcha_ideal.settings')->get('persistence');
    if ($config_persistance_ideal == 0) {
      $config_persistance_ideal = 'false';
    };
    if ($config_persistance_ideal == 1) {
      $config_persistance_ideal = 'false';
    };
    if ($config_persistance_ideal == 2) {
      $config_persistance_ideal = 'true';
    };
    if ($config_persistance_ideal == 2) {
      $config_persistance_ideal = 'false';
    };
    // Captcha based on .yml file.
    // 1.
    $config_enableform = $configFactory->get('captcha.settings')->get('enable_globally');
    if ($config_enableform == 0) {
      $config_enableform = 'false';
    };
    if ($config_enableform == 1) {
      $config_enableform = 'true';
    };
    // 0
    $config_casesensitive = $configFactory->get('captcha.settings')->get('default_validation');
    if ($config_casesensitive == 0) {
      $config_casesensitive = 'true';
    };
    if ($config_casesensitive == 1) {
      $config_casesensitive = 'false';
    };
    // 2
    $config_persistance = $configFactory->get('captcha.settings')->get('persistence');
    if ($config_persistance == 0) {
      $config_persistance = 'false';
    };
    if ($config_persistance == 1) {
      $config_persistance = 'false';
    };
    if ($config_persistance == 2) {
      $config_persistance = 'true';
    };
    if ($config_persistance == 2) {
      $config_persistance = 'false';
    };

    // PASSWORD POLICY IDEAL.
    $pass_reset_ideal = $configFactory->get('password_policy_ideal.settings')->get('password_reset');
    $pass_length_ideal = $configFactory->get('password_policy_ideal.settings')->get('password_length');
    $pass_username_ideal = $configFactory->get('password_policy_ideal.settings')->get('password_username');
    if ($pass_username_ideal == 0) {
      $pass_username_ideal = 'true';
    };
    if ($pass_username_ideal == 1) {
      $pass_username_ideal = 'false';
    };
    $pass_character_ideal = $configFactory->get('password_policy_ideal.settings')->get('character_types');
    $repeat_pass_ideal = $configFactory->get('password_policy_ideal.settings')->get('password_policy_history_constraint');

    // Retrieve the security review checklist results.
    $results = $this->securityReviewResults();
    $results_sec_review = [];
    if ($results == NULL) {
      $results_sec_review = 'Security Review scanner was not run for this website, please go to configuration page of module Security Review after installation and run it to have this information available!';
    }

    if ($results != NULL) {
      $ct_ok = 0;
      $ct_no = 0;
      $ct_total = $ct_no + $ct_ok;
      for ($x = 0; $x < count($results); $x++) {
        if ($results[$x][0] == '0') {
          $results[$x][0] = 'OK';
          $ct_ok = $ct_ok + 1;
        }
        if ($results[$x][0] == '1' || $results[$x][0] == '2') {
          $results[$x][0] = 'Not ok';
          $ct_no = $ct_no + 1;
        }
        $aux_res = [(string) $results[$x][1], $results[$x][0]];
        $results_sec_review = array_merge($results_sec_review, [$aux_res]);
      }
    }

    // Table build.
    $header = [
      'col1' => t('Module'),
      'col2' => t('Status'),
    ];

    $header_skit = [
      'col1' => t('Security Parameter'),
      'col2' => t('Actual Value'),
      'col3' => t('Advised Value'),
      'col4' => t('Ok/Not ok'),
    ];

    $rows_basic = [
      ['Security Review', $status_sec_review],
      ['EU Cookies Policy', $module_cookies_policy],
      ['Password Policy', $module_pass_policy],
      ['Security kit', $module_seckit],
      ['CAPTCHA or reCAPTCHA',
        ($module_captcha == $module_recaptcha and $module_captcha == 'NOT INSTALLED') ? 'NOT INSTALLED' : 'INSTALLED',
      ],
      ['Login Security', $module_log_sec],
    ];

    if ($module_seckit == 'NOT INSTALLED') {
      $rows_skit = 'The security KIT module is not installed, please install to analyse website for header policies regading cross-site scripting, clicjacking, SSL, referrer policy etc';
    }
    else {
      $rows_skit = [
        ['Xframe Options header',
          $config_xframe,
          $config_xframe_ideal,
          ($config_xframe === $config_xframe_ideal) ? 'OK' : 'Not ok',
        ],
        ['X-Xss Protection header ',
          $config_xss, $config_xss_ideal,
          ($config_xss === $config_xss_ideal) ? 'OK' : 'Not ok',
        ],
        ['CSP - Send CSP HTTP response header',
          $config_csp ? 'true' : 'false',
          $config_csp_ideal ? 'true' : 'false',
          ($config_csp ? $config_csp_ideal : 'false' === 'true') ? 'OK' : 'Not ok',
        ],
        ['SSL - Enable SSL HTTP response header',
          $config_ssl_transport ? 'true' : 'false',
          $config_ssl_transport_ideal ? 'true' : 'false',
          ($config_ssl_transport ? $config_ssl_transport_ideal : 'false' === 'true') ? 'OK' : 'Not ok',
        ],
        ['SSL - Max-Age',
          $config_ssl_maxage, $config_ssl_maxage_ideal,
          ($config_ssl_maxage <= $config_ssl_maxage_ideal) ? 'OK' : 'Not ok',
        ],
        ['SSL - Force HTTP SSL for all subdomains',
          $config_ssl_subdomains ? 'true' : 'false',
          $config_ssl_subdomains_ideal ? 'true' : 'false',
          ($config_ssl_subdomains ? $config_ssl_subdomains_ideal : 'false' === 'true') ? 'OK' : 'Not ok',
        ],
        ['Miscellaneous - Referrer policy',
          $config_miscellaneous_referrer ? 'true' : 'false',
          $config_miscellaneous_referrer_ideal ? 'true' : 'false',
          ($config_miscellaneous_referrer ? $config_miscellaneous_referrer_ideal : 'false' === 'true') ? 'OK' : 'Not ok',
        ],
        ['Miscellaneous - Referrer policy Value',
          $config_miscellaneous_referrer_value,
          $config_miscellaneous_referrer_value_ideal,
          ($config_miscellaneous_referrer_value === $config_miscellaneous_referrer_value_ideal) ? 'OK' : 'Not ok',
        ],
        ['Miscellaneous - Disable autocomplete ',
          $config_miscellaneous_autocomplete ? 'true' : 'false',
          $config_miscellaneous_autocomplete_ideal ? 'true' : 'false',
          ($config_miscellaneous_autocomplete ? $config_miscellaneous_autocomplete_ideal : 'false' === 'true') ? 'OK' : 'Not ok',
        ],
      ];
    }

    if ($module_pass_policy == 'NOT INSTALLED') {
      $rows_passpolicy = 'The module Password policy is not installed, please consider also the instalation of modules to properly define policies regarding minimim number and type of caracteres, use username etc';
    }
    else {
      $passpolicy_results = $this->passwordPolicyResults();
      $rows_passpolicy = [
        ['Password reset time',
          $passpolicy_results["password_reset"],
          $pass_reset_ideal,
          ($passpolicy_results["password_reset"] <= $pass_reset_ideal) ? 'OK' : 'Not ok',
        ],
        ['Minimum Password Length',
          $passpolicy_results["password_length"],
          $pass_length_ideal,
          ((int) $passpolicy_results["password_length"] >= $pass_length_ideal) ? 'OK' : 'Not ok',
        ],
        ['Use username in password',
          (string) $passpolicy_results["password_username"],
          $pass_username_ideal,
          ($passpolicy_results["password_username"] == $pass_username_ideal) ? 'OK' : 'Not ok',
        ],
        ['Types of characters (eg. A,a,1,#)',
          $passpolicy_results["character_types"],
          $pass_character_ideal,
          ((int) $passpolicy_results["character_types"] >= $pass_character_ideal) ? 'OK' : 'Not ok',
        ],
        ['Repeat old passwords',
          $passpolicy_results["password_policy_history_constraint"],
          $repeat_pass_ideal,
          ($passpolicy_results["password_policy_history_constraint"] <= $repeat_pass_ideal) ? 'OK' : 'Not ok',
        ],
      ];
    }

    if ($module_log_sec == 'NOT INSTALLED') {
      $rows_loginsec = 'The module Login Security is not installed, please install to define the maximum number of login attemps, time to retry login etc ';
    }
    else {
      $rows_loginsec = [
        ['Track time after attempts failed',
          $config_tracktime,
          $config_tracktime_ideal,
          ($config_tracktime <= $config_tracktime_ideal) ? 'OK' : 'Not ok',
        ],
        ['Maximum Login attemps',
          $config_softhost, $config_softhost_ideal,
          ($config_softhost <= $config_softhost_ideal) ? 'OK' : 'Not ok',
        ],
        ['Warning entry log after attempts failed',
          $config_activity,
          $config_activity_ideal,
          ($config_activity <= $config_activity_ideal) ? 'OK' : 'Not ok',
        ],
      ];
    }

    if ($module_captcha == 'NOT INSTALLED' && $module_recaptcha == 'NOT INSTALLED') {
      $rows_captcha = 'The module Captcha (or recaptcha) is not installed, please install it (or equivalent one) to ensure human website usage';
    }
    elseif ($module_captcha == 'INSTALLED') {
      $rows_captcha = [
        ['Enable CAPTCHA for every form',
          $config_enableform,
          $config_enableform_ideal,
          ($config_enableform == $config_enableform_ideal) ? 'OK' : 'Not ok',
        ],
        ['Case sensitive validation',
          $config_casesensitive,
          $config_casesensitive_ideal,
          ($config_casesensitive == $config_casesensitive_ideal) ? 'OK' : 'Not ok',
        ],
        ['Omit a form type challenge if passed on same session',
          $config_persistance,
          $config_persistance_ideal,
          ($config_persistance == $config_persistance_ideal) ? 'OK' : 'Not ok',
        ],
      ];
    }

    $rows_sreview = $results_sec_review;

    $data_graphic = [$ct_ok, $ct_no];

    $links_ref = ['https://www.drupal.org/project/security_review', 'https://www.drupal.org/project/eu_cookie_compliance', 'https://www.drupal.org/project/password_policy',
      'https://www.drupal.org/project/seckit', 'https://www.drupal.org/project/captcha', 'https://www.drupal.org/project/login_security',
    ];

    $links_config = ['/admin/config/security-review', '/admin/config/system/eu-cookie-compliance', '/admin/config/security/password-policy',
      '/admin/config/system/seckit', '/admin/config/people/captcha', '/admin/config/people/login_security',
    ];

    $temp_dir = \Drupal::service('file_system')->getTempDirectory();
    $current_date = date('_Y_m_d_H_i');
    $filename = '/security_analysis' . $current_date . '.xlsx';
    $file_path = $temp_dir . $filename;

    $result = [
      'header' => $header,
      'rows_basic' => $rows_basic,
      'header_skit' => $header_skit,
      'rows_skit' => $rows_skit,
      'rows_passpolicy' => $rows_passpolicy,
      'rows_loginsec' => $rows_loginsec,
      'rows_captcha' => $rows_captcha,
      'rows_sreview' => $rows_sreview,
      'data_graphic' => $data_graphic,
      'links_ref' => $links_ref,
      'links_config' => $links_config,
      'file_path' => $file_path,
    ];

    return $result;
  }

  /**
   * Method to build page html.
   */
  public function content() {
    $buildData = $this->buildata();
    $content = [];

    $link2 = Url::fromRoute('security_analysis_download_button.download_file_excel');
    $content['link'] = [
      '#type' => 'link',
      '#title' => $this->t('&#11015   CLICK HERE to download Excel File with website Security Analysis information'),
      '#url' => $link2,
      '#attributes' => [
        'class' => ['button'],
        'style' => 'background-color: #070F26; color: #fff; transition: background-color 0.3s ease;',
      ],
    ];

    $content['info_status'] = [
      '#theme' => 'security_analysis_list',
      '#rows' => $buildData['rows_basic'],
      '#links_ref' => $buildData['links_ref'],
      '#links_config' => $buildData['links_config'],
    ];

    $content['info_skit'] = [
      '#theme' => 'security_analysis_table',
      '#title' => 'Security Kit Module Parameters',
      '#header' => $buildData['header_skit'],
      '#rows' => $buildData['rows_skit'],
    ];

    $content['info_loginsec'] = [
      '#theme' => 'security_analysis_id',
      '#title' => 'Login Security Module Parameters',
      '#header' => $buildData['header_skit'],
      '#rows' => $buildData['rows_loginsec'],
    ];

    $content['info_passpolicy'] = [
      '#theme' => 'security_analysis_id',
      '#title' => 'Password Policy Module Parameters',
      '#header' => $buildData['header_skit'],
      '#rows' => $buildData['rows_passpolicy'],
    ];

    $content['info_captcha'] = [
      '#theme' => 'security_analysis_id',
      '#title' => 'Captcha Module Parameters',
      '#header' => $buildData['header_skit'],
      '#rows' => $buildData['rows_captcha'],
    ];

    $content['info_sreview'] = [
      '#theme' => 'security_analysis_plot',
      '#title' => 'Security Review Module Parameters',
      '#header' => $buildData['header'],
      '#rows' => $buildData['rows_sreview'],
      '#data' => $buildData['data_graphic'],
    ];

    $link_doc = Url::fromUri('https://www.youtube.com/watch?app=desktop&v=hyXSKl4hipA&list=PLpeDXSh4nHjTALaf6oy_sIq6PryRzxKup&index=46');
    $content['doc_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Click here to know more about security by design document'),
      '#url' => $link_doc,
      '#attributes' => [
        'target' => ['blank'],
        'class' => ['button'],
        'style' => 'background-color: #070F26; color: #fff; transition: background-color 0.3s ease;',
      ],
    ];

    $content['#attached']['drupalSettings']['piedata'] = $buildData['data_graphic'];

    return $content;
  }

}
